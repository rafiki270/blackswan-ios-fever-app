//
//  NSHipsterUITests.m
//  NSHipsterUITests
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>


@interface NSHipsterUITests : XCTestCase

@property (nonatomic, strong) XCUIApplication *app;

@end


@implementation NSHipsterUITests

- (void)setUp {
    [super setUp];
    
    self.continueAfterFailure = NO;
    
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationPortrait;
    [XCUIDevice sharedDevice].orientation = UIDeviceOrientationPortrait;
    
    self.app = [[XCUIApplication alloc] init];
    self.app.launchArguments = @[@"UI_TESTING_MODE"];
    [self.app launch];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testUserCanClickOnItem {
    [self.app.tables.staticTexts[@"Test item"] tap];
    
    XCTAssert(self.app.staticTexts[@"Test item"].exists);
    XCTAssert(self.app.staticTexts[@"Test summary"].exists);
}

@end
