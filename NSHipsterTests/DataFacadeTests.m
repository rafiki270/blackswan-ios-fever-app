//
//  DataFacadeTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "BSWDataFacade.h"
#import "MWFeedParser.h"
#import "BSWConfig.h"
#import "NSURL+uriEquivalence.h"


@interface BSWDataFacade () <MWFeedParserDelegate>

@property (nonatomic, strong, readwrite) MWFeedParser *parser;
@property (nonatomic, strong, readwrite) NSMutableArray *data;

@end


@interface DataFacadeTests : XCTestCase

@property (nonatomic, strong) BSWDataFacade *facade;

@end


@implementation DataFacadeTests

- (void)setUp {
    [super setUp];
    
    _facade = [[BSWDataFacade alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testFeedParserIsNotNil {
    XCTAssertNotNil(_facade.parser);
}

- (void)testDataIsNotNil {
    XCTAssertNotNil(_facade.data);
}

- (void)testParserHasTheRightUrl {
    XCTAssertTrue([_facade.parser.url isEquivalent:[NSURL URLWithString:[BSWConfig feedUrl]]]);
}

- (void)testParserHasDelegate {
    XCTAssertEqual(_facade.parser.delegate, _facade);
}

- (void)testParserIsConfiguredProperly {
    XCTAssertTrue(_facade.parser.feedParseType == ParseTypeFull);
    XCTAssertTrue(_facade.parser.connectionType == ConnectionTypeSynchronously);
}

- (void)testParserDelegateAddsItemToData {
    [self addMockData];
    
    XCTAssertTrue(_facade.data.count == 2);
}

- (void)testParserDelegateSendsDataInBlockWhenDone {
    __block BOOL ok = false;
    __block NSInteger count = 0;
    [self addMockData];
    
    [_facade setDidLoadData:^(NSArray *data) {
        ok = true;
        count = data.count;
    }];
    [_facade feedParserDidFinish:_facade.parser];
    
    XCTAssertTrue(ok);
    XCTAssertTrue(count == 2);
}

#pragma mark Helpers

- (void)addMockData {
    MWFeedItem *item1 = OCMClassMock([MWFeedItem class]);
    MWFeedItem *item2 = OCMClassMock([MWFeedItem class]);
    
    [_facade feedParser:_facade.parser didParseFeedItem:item1];
    [_facade feedParser:_facade.parser didParseFeedItem:item2];
}


@end
