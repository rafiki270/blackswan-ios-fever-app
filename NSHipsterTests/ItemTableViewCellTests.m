//
//  ItemTableViewCellTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 15/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BSWItemTableViewCell.h"
#import "MWFeedParser.h"


@interface ItemTableViewCellTests : XCTestCase

@property (nonatomic, strong, readwrite) BSWItemTableViewCell *cell;

@end


@implementation ItemTableViewCellTests

- (void)setUp {
    [super setUp];
    
    _cell = [[BSWItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"identifier"];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testItemCanBeSet {
    MWFeedItem *item = [[MWFeedItem alloc] init];
    [_cell setItem:item];
    
    XCTAssertTrue(_cell.item == item);
}

- (void)testValuesAreDisplayedWhenItemIsSet {
    MWFeedItem *item = [[MWFeedItem alloc] init];
    [item setTitle:@"Title"];
    [item setAuthor:@"Author"];
    [_cell setItem:item];
    
    XCTAssertEqual(_cell.textLabel.text, _cell.item.title);
    XCTAssertEqual(_cell.detailTextLabel.text, _cell.item.author);
}

- (void)testAllIsSetProperlyAfterInitialization {
    XCTAssertTrue(_cell.accessoryType == UITableViewCellAccessoryDisclosureIndicator);
}


@end
