//
//  MockTableView.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 15/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MockTableView : UITableView

@property (nonatomic) NSInteger reloadCount;


@end
