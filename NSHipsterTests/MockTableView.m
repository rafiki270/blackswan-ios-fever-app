//
//  MockTableView.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 15/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "MockTableView.h"


@implementation MockTableView


#pragma mark Mock methods

- (void)reloadData {
    _reloadCount++;
}


@end
