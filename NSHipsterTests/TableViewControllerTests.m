//
//  TableViewControllerTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BSWTableViewController.h"


@interface TableViewControllerTests : XCTestCase

@property (nonatomic, strong) BSWTableViewController *tableViewController;

@end


@implementation TableViewControllerTests

- (void)setUp {
    [super setUp];
    
    _tableViewController = [[BSWTableViewController alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

/*
If I'd be using specta/expecta, the following test would look like this (unfortunately I have some compile errors so will have to stick to XCTest):
 
 SpecBegin(BSWTableViewController)
   describe(@"BSWTableViewController", ^{
     it(@"should be subclass of table view controller", ^{
       BSWTableViewController *controller = [[BSWTableViewController alloc] init];
       expect([controller isKindOfClass:[UITableViewController class]]).to.equal(YES);
     });
   });
 
 */

- (void)testControllerIsSubclassOfBaseTableViewController {
    XCTAssertTrue([_tableViewController isKindOfClass:[UITableViewController class]]);
}


@end
