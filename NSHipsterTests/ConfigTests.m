//
//  ConfigTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BSWConfig.h"


@interface ConfigTests : XCTestCase

@end


@implementation ConfigTests


- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testExample {
    XCTAssertEqualObjects([BSWConfig feedUrl], @"http://nshipster.com/feed.xml");
}


@end
