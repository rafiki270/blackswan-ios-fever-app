//
//  HomeControllerTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MWFeedParser.h"
#import "OCMock.h"
#import "BSWHomeController.h"
#import "BSWDataFacade.h"
#import "MockDataFacade.h"
#import "MockTableView.h"
#import "BSWItemTableViewCell.h"


@interface BSWHomeController ()

@property (nonatomic, strong, readwrite) BSWDataFacade *dataFacade;
@property (nonatomic, strong, readwrite) NSArray *data;
@property (nonatomic, copy) void (^didLoadData)(NSArray *data);

- (void)setDidLoadDataBlock;

@end


@interface HomeControllerTests : XCTestCase

@property (nonatomic, strong) BSWHomeController *controller;

@end


@implementation HomeControllerTests

- (void)setUp {
    [super setUp];
    
    _controller = [[BSWHomeController alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testFacadeExists {
    XCTAssertNotNil(_controller.dataFacade);
}

- (void)testFacadeHasDidFinishBlock {
    XCTAssertNotNil(_controller.dataFacade.didLoadData);
}

- (void)testDataFilledWhenFacadeReturnsData {
    [self setupMockFacade];
    
    [_controller reloadData];
    [_controller reloadData];
    [_controller reloadData];
    
    XCTAssertTrue(_controller.data.count == 3);
}

- (void)testTableHasRightAmountOfSections {
    UITableView *t = [[UITableView alloc] init];
    XCTAssertTrue([_controller numberOfSectionsInTableView:t] == 1);
}

- (void)testTableHasRightAmountOfRows {
    _controller.data = @[@"", @""];
    UITableView *t = [[UITableView alloc] init];
    
    XCTAssertTrue([_controller tableView:t numberOfRowsInSection:0] == 2);
}

- (void)testCellIsNotNil {
    [self setupMockFacade];
    [_controller reloadData];
    
    UITableView *t = [[UITableView alloc] init];
    UITableViewCell *cell = [_controller tableView:t cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    XCTAssertNotNil(cell);
}

- (void)testRightTableViewCellIsBeingCreated {
    [self setupMockFacade];
    [_controller reloadData];
    
    UITableView *t = [[UITableView alloc] init];
    UITableViewCell *cell = [_controller tableView:t cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    XCTAssertTrue([cell isKindOfClass:[BSWItemTableViewCell class]]);
}

- (void)testCellHasTheRightDataInIt {
    [self setupMockFacade];
    
    [_controller reloadData];
    [_controller reloadData];
    [_controller reloadData];
    
    UITableView *t = [[UITableView alloc] init];
    BSWItemTableViewCell *cell = (BSWItemTableViewCell *)[_controller tableView:t cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    XCTAssertTrue(cell.item == _controller.data[1]);
}

- (void)testTableViewGetsReloadedWhenDataComesThrough {
    MockTableView *tableMock = [[MockTableView alloc] init];
    MockDataFacade *facadeMock = [[MockDataFacade alloc] init];
    
    _controller.dataFacade = facadeMock;
    _controller.tableView = tableMock;
    
    [_controller setDidLoadDataBlock];
    
    [_controller reloadData];
    
    XCTAssertTrue([(MockTableView *)_controller.tableView reloadCount] == 1);
}

- (void)testItemForIndexPathReturnsTheRightItem {
    MWFeedItem *i1 = [[MWFeedItem alloc] init];
    MWFeedItem *i2 = [[MWFeedItem alloc] init];
    
    _controller.data = @[i1, i2];
    
    XCTAssertEqualObjects(i1, [_controller itemForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]);
    XCTAssertEqualObjects(i2, [_controller itemForItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]);
}

#pragma mark Helper methods

- (void)setupMockFacade {
    MockDataFacade *mock = [[MockDataFacade alloc] init];
    
    _controller.dataFacade = mock;
    __weak typeof(self) weakSelf = self;
    [_controller.dataFacade setDidLoadData:^(NSArray *data) {
        weakSelf.controller.data = data;
    }];
}


@end
