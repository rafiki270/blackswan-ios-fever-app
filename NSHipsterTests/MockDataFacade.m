//
//  MockDataFacade.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "MockDataFacade.h"
#import "MWFeedParser.h"
#import "OCMock.h"


@interface MockDataFacade ()

@property (nonatomic, strong, readwrite) NSMutableArray *fakeData;

@end


@implementation MockDataFacade


#pragma mark Mock methods

- (void)getLatestData {
    if (!_fakeData) {
        _fakeData = [NSMutableArray array];
    }
    
    MWFeedItem *item = OCMClassMock([MWFeedItem class]);
    [_fakeData addObject:item];
    
    if (self.didLoadData) {
        self.didLoadData(self.fakeData);
    }
}


@end
