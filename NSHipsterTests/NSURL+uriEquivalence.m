//
//  NSURL+uriEquivalence.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "NSURL+uriEquivalence.h"


@implementation NSURL (uriEquivalence)


- (BOOL)isEquivalent:(NSURL *)aURL {
    if ([self isEqual:aURL]) return YES;
    if ([[self scheme] caseInsensitiveCompare:[aURL scheme]] != NSOrderedSame) return NO;
    if ([[self host] caseInsensitiveCompare:[aURL host]] != NSOrderedSame) return NO;

    if ([[self path] compare:[aURL path]] != NSOrderedSame) return NO;
    
    if ([self port] || [aURL port]) {
        if (![[self port] isEqual:[aURL port]]) return NO;
        if (![[self query] isEqual:[aURL query]]) return NO;
    }
    
    return YES;
}


@end
