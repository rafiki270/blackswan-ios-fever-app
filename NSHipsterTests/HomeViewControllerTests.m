//
//  HomeViewControllerTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "BSWHomeController.h"
#import "BSWHomeViewController.h"
#import "BSWDetailViewController.h"
#import "MWFeedParser.h"


@interface BSWHomeViewController ()

@property (nonatomic, strong, readwrite) BSWHomeController *controller;

@end


@interface BSWHomeController ()

@property (nonatomic, strong, readwrite) NSArray *data;

@end


@interface BSWHomeController ()

- (void)reloadData;

@end


@interface HomeViewControllerTests : XCTestCase

@property (nonatomic, strong) BSWHomeViewController *homeViewController;

@end


@implementation HomeViewControllerTests

- (void)setUp {
    [super setUp];
    
    _homeViewController = [[BSWHomeViewController alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testControllerIsSubclassOfBaseTableViewController {
    XCTAssertTrue([_homeViewController isKindOfClass:[BSWTableViewController class]]);
}

- (void)testTitleIsSet {
    if (_homeViewController.view) { } // Touching the view
    
    XCTAssertEqual(_homeViewController.title, @"NSHipster");
}

- (void)testControllerExists {
    XCTAssertNotNil(_homeViewController.controller);
    XCTAssertTrue([_homeViewController.controller isKindOfClass:[BSWHomeController class]]);
}

- (void)testControllerHasReferenceToTableView {
    XCTAssertTrue(_homeViewController.controller.tableView == _homeViewController.tableView);
}

- (void)testDataIsBeingLoadedOnViewWillAppear {
    id mock = OCMClassMock([BSWHomeController class]);
    
    _homeViewController.controller = mock;
    [_homeViewController viewWillAppear:YES];
    
    OCMVerify([mock reloadData]);
}

- (void)testTableViewHasTheRightDataSource {
    XCTAssertTrue(_homeViewController.controller == _homeViewController.tableView.dataSource); // Needs to be in this order for the controller to be created
}

- (void)testTableViewHasTheRightDelegate {
    XCTAssertTrue(_homeViewController.tableView.delegate == _homeViewController);
}

- (void)testTableViewCellIsDeselectedAfterCellIsSelected {
    id mock = OCMClassMock([UITableView class]);
    _homeViewController.tableView = mock;
    NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [_homeViewController tableView:mock didSelectRowAtIndexPath:ip];
    
    OCMVerify([mock deselectRowAtIndexPath:ip animated:YES]);
}

- (void)testDetailViewControllerIsPushedOnRowClickAndThatFeedItemIsPassed {
    NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:_homeViewController];
    if (nc) { }
    _homeViewController.controller.data = @[[[MWFeedItem alloc] init]];
    
    [_homeViewController tableView:[[UITableView alloc] init] didSelectRowAtIndexPath:ip];
    
    XCTestExpectation *completionExpectation = [self expectationWithDescription:@"WaitingForPush"];
    double delayInSeconds = 4.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, popTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        BSWDetailViewController *detail = (BSWDetailViewController *)nc.viewControllers[1];
        
        // Testing feed item
        XCTAssertTrue(detail.item == [_homeViewController.controller itemForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]);
        
        // Testing navigation stack
        XCTAssertTrue(nc.viewControllers.count == 2);
        XCTAssertTrue([detail isKindOfClass:[BSWDetailViewController class]]);
        
        [completionExpectation fulfill];
    });
    [self waitForExpectationsWithTimeout:(delayInSeconds + 2) handler:nil];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
