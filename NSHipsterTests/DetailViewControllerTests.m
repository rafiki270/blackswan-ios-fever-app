//
//  DetailViewControllerTests.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 17/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OCMock.h"
#import "MWFeedParser.h"
#import "BSWDetailViewController.h"


@interface BSWDetailViewController ()

@property (nonatomic, strong, readwrite) UIWebView *webView;

@end


@interface DetailViewControllerTests : XCTestCase

@property (nonatomic, strong) BSWDetailViewController *detailViewController;

@end


@implementation DetailViewControllerTests


- (void)setUp {
    [super setUp];
    
    _detailViewController = [[BSWDetailViewController alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testWebViewIsCreated {
    XCTAssertNotNil(_detailViewController.webView);
}

- (void)testWebViewHasSuperView {
    if (_detailViewController.view) { } // Touch view
    
    XCTAssertEqualObjects(_detailViewController.webView.superview, _detailViewController.view);
}

- (void)testItemIsRetained {
    MWFeedItem *item = [[MWFeedItem alloc] init];
    _detailViewController.item = item;
    
    XCTAssertEqualObjects(_detailViewController.item, item);
}

- (void)testTitleIsSet {
    MWFeedItem *item = [[MWFeedItem alloc] init];
    item.title = @"Test title";
    _detailViewController.item = item;
    
    XCTAssertEqualObjects(_detailViewController.title, item.title);
}

- (void)testContentIsLoaded {
    MWFeedItem *item = [[MWFeedItem alloc] init];
    item.summary = @"Test summary";
    _detailViewController.item = item;
    
    id mock = OCMClassMock([UIWebView class]);
    _detailViewController.webView = mock;
    
    [_detailViewController viewWillAppear:YES];
    
    OCMVerify([mock loadHTMLString:_detailViewController.item.summary baseURL:nil]);
}


@end
