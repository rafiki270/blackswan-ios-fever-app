//
//  BSWHomeviewController.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWHomeViewController.h"
#import "BSWDetailViewController.h"
#import "BSWHomeController.h"


@interface BSWHomeViewController () <UITableViewDelegate>

@property (nonatomic, strong, readwrite) BSWHomeController *controller;

@end


@implementation BSWHomeViewController


#pragma mark Properties

- (BSWHomeController *)controller {
    if (!_controller) {
        _controller = [[BSWHomeController alloc] init];
        [_controller setTableView:self.tableView];
        
        [self.tableView setDataSource:_controller];
        [self.tableView setDelegate:self];
    }
    return _controller;
}

#pragma mark View lifecycle

- (void)loadView {
    [super loadView];
    
    [self setTitle:@"NSHipster"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.controller reloadData];
}

#pragma mark Table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BSWDetailViewController *c = [[BSWDetailViewController alloc] init];
    c.item = [self.controller itemForItemAtIndexPath:indexPath];
    [self.navigationController pushViewController:c animated:YES];
}


@end
