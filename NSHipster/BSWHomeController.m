//
//  BSWHomeController.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWHomeController.h"
#import "BSWDataFacade.h"
#import "BSWItemTableViewCell.h"


typedef void (^BSWHomeControllerDidLoadDataBlock)(NSArray *data);


@interface BSWHomeController ()

@property (nonatomic, strong, readwrite) BSWDataFacade *dataFacade;
@property (nonatomic, strong, readwrite) NSArray *data;
@property (nonatomic, copy) void (^didLoadData)(NSArray *data);

@end


@implementation BSWHomeController


#pragma mark Settings

- (void)reloadData {
    [self.dataFacade getLatestData];
}

#pragma mark Properties

- (void)setDidLoadDataBlock {
    __weak typeof(self) weakSelf = self;
    [self setDidLoadData:^(NSArray *data) {
        weakSelf.data = data;
        [weakSelf.tableView reloadData];
    }];
    [_dataFacade setDidLoadData:self.didLoadData];
}

- (BSWDataFacade *)dataFacade {
    if (!_dataFacade) {
        _dataFacade = [[BSWDataFacade alloc] init];
        
        [self setDidLoadDataBlock];
    }
    return _dataFacade;
}

#pragma mark Data

- (MWFeedItem *)itemForItemAtIndexPath:(NSIndexPath *)indexPath {
    MWFeedItem *item = self.data[indexPath.row];
    return item;
}

#pragma mark Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cellId";
    BSWItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[BSWItemTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    MWFeedItem *item = [self itemForItemAtIndexPath:indexPath];
    [cell setItem:item];
    
    return cell;
}


@end
