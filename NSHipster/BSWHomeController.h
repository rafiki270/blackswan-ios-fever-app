//
//  BSWHomeController.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MWFeedItem;

@interface BSWHomeController : NSObject <UITableViewDataSource>


@property (nonatomic, weak) UITableView *tableView;

- (MWFeedItem *)itemForItemAtIndexPath:(NSIndexPath *)indexPath;
- (void)reloadData;


@end
