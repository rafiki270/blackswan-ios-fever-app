//
//  BSWDetailViewController.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 17/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWDetailViewController.h"
#import "Masonry.h"
#import "MWFeedParser.h"


@interface BSWDetailViewController ()

@property (nonatomic, strong, readwrite) UIWebView *webView;

@end


@implementation BSWDetailViewController


#pragma mark Layout

- (void)layoutWebView {
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark Settings

- (void)setItem:(MWFeedItem *)item {
    _item = item;
    
    self.title = item.title;
}

#pragma mark Properties

- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] init];
    }
    return _webView;
}

#pragma mark Creating elements

- (void)addWebView {
    [self.view addSubview:self.webView];
}

#pragma mark View lifecycle

- (void)loadView {
    [super loadView];
    
    [self addWebView];
    [self layoutWebView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.webView loadHTMLString:self.item.summary baseURL:nil];
}


@end
