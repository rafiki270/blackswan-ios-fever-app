//
//  BSWConfig.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BSWConfig : NSObject

+ (NSString *)feedUrl;
+ (void)configureColors;


@end
