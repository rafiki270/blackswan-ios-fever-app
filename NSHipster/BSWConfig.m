//
//  BSWConfig.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWConfig.h"
#import <UIKit/UIKit.h>


@implementation BSWConfig


#pragma mark Config

+ (NSString *)feedUrl {
    return @"http://nshipster.com/feed.xml";
}

#pragma mark Styling

+ (void)configureColors {
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:(245.0/255.0) green:(126.0/255.0) blue:(0.0/255.0) alpha:1]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName: [UIColor colorWithRed:(51.0/255.0) green:(51.0/255.0) blue:(51.0/255.0) alpha:1]
                                                           }];
}


@end
