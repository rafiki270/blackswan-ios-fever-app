//
//  AppDelegate.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

