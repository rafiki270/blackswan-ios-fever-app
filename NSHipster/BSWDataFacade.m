//
//  BSWDataFacade.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWDataFacade.h"
#import "MWFeedParser.h"
#import "BSWConfig.h"


@interface BSWDataFacade () <MWFeedParserDelegate>

@property (nonatomic, strong, readwrite) MWFeedParser *parser;
@property (nonatomic, strong, readwrite) NSMutableArray *data;

@end


@implementation BSWDataFacade


#pragma mark Properties

- (MWFeedParser *)parser {
    if (!_parser) {
        if (![[NSProcessInfo processInfo].arguments containsObject:@"UI_TESTING_MODE"]) {
            NSURL *url = [NSURL URLWithString:[BSWConfig feedUrl]];
            _parser = [[MWFeedParser alloc] initWithFeedURL:url];
            
            [_parser setDelegate:self];
            [_parser setFeedParseType:ParseTypeFull];
            [_parser setConnectionType:ConnectionTypeSynchronously];
        }
        else {
            NSLog(@"UI testing mode");
            
            if (self.didLoadData) {
                MWFeedItem *item = [[MWFeedItem alloc] init];
                item.title = @"Test item";
                item.author = @"Ondrej Rafaj";
                item.summary = @"Test summary";
                self.didLoadData(@[item]);
            }
        }
    }
    return _parser;
}

- (NSMutableArray *)data {
    if (!_data) {
        _data = [NSMutableArray array];
    }
    return _data;
}

#pragma mark Pulling data

- (void)getLatestData {
    [self.data removeAllObjects];
    
    [self.parser parse];
}

#pragma mark Parser delegate methods

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    [self.data addObject:item];
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
    if (self.didLoadData) {
        self.didLoadData(self.data);
    }
}


@end
