//
//  BSWDataFacade.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BSWDataFacade : NSObject

@property (nonatomic, copy) void (^didLoadData)(NSArray *data);

- (void)getLatestData;


@end
