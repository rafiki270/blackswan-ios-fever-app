//
//  BSWHomeviewController.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 14/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWTableViewController.h"

@interface BSWHomeViewController : BSWTableViewController

@end
