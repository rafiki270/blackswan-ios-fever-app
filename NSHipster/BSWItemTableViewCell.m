//
//  BSWItemTableViewCell.m
//  NSHipster
//
//  Created by Ondrej Rafaj on 15/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import "BSWItemTableViewCell.h"
#import "MWFeedParser.h"


@implementation BSWItemTableViewCell


#pragma mark Settings

- (void)setItem:(MWFeedItem *)item {
    _item = item;
    
    self.textLabel.text = _item.title;
    self.detailTextLabel.text = _item.author;
}

#pragma mark Initialization

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return self;
}

@end
