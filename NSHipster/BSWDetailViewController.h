//
//  BSWDetailViewController.h
//  NSHipster
//
//  Created by Ondrej Rafaj on 17/04/2016.
//  Copyright © 2016 Ridiculous Innovations. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MWFeedItem;

@interface BSWDetailViewController : UIViewController

@property (nonatomic, strong, readwrite) MWFeedItem *item;


@end
