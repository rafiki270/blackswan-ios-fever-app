# NSHipster Fever app

#### Description
App is built on a modified MVC (MVVC - Model View ViewController) architecture. Other paterns like VIPER were considered but due to a limited time to build this project they are not a viable option.

The new content notifications haven't been implemented due to time limitations but happy to look into it later if required.

#### Installation
The pods are part of the git repository, the app should be ready to go, right after clonning.

#### Third-party code
Only neccessary and well tested thirdparty libraries are used in this project. MWFeedPArser doesn't contain tests but has been proven to be a very good resource in thousands of projects and is very reliable

#### Tests
I have chosen full TDD approach but happy with any form TDD/BDD/Core testing
-testDetailViewControllerIsPushedOnRowClickAndThatFeedItemIsPassed on detail view controller tests sometimes fails, probably some race condition that would need to be debugged

#### Continuous integration
NSHipster has an XCode server integration client here: https://build.wba2.com/
Note: Shutterjobs is a test project of a friend :) ... I don't have warnings in my code :)))

#### Contact
Ondrej Rafaj
07972574949
Skype: rafiki270
Email: ondrej.rafaj@gmail.com

---------------------------------------------


## Original

#### BlackSwan iOS Fever app

Show us what you can do and how clean your code is! Write an app that uses public APIs to fetch data and display it in an organized way.

Even if we all love playing with Swift (who doesn't ;)), please stick with the plain old Objective-C here.

For example:

* A StackOverflow client that shows the hottest questions
* An app that shows random images of cats from [The Cat API](http://thecatapi.com/)
* An app that downloads the NSHipster RSS feed showing and index of all the articles and that alerts the user when the latest article is published
* _Anything you can come up with!_

We could be sneaky and not say anything else, but here's some things we're looking to see:

* Use of existing open source libraries
* Standard best practises for Networking - Not reiventing the wheel
* Tests!
* **In this test** we don't care too much about the UI, what we care is how good the code is!

### Submission notes

You can just submit a PR here, create a private repo for free on [GitLab](https://www.gitlab.com/?gclid=CLCBmaWM474CFaMSwwodAqIAqw) or [Bitbucket](https://bitbucket.org/), or just send us the repo by email. Whatever you prefer.

---

[@BlackSwan](https://www.bkakswan.com) - 2014
